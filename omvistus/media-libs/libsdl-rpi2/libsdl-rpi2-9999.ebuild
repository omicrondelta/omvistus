# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5

inherit eutils git-r3

DESCRIPTION="Simple Direct Media Layer RPI patched"
HOMEPAGE="http://www.libsdl.org/"
EGIT_REPO_URI="https://github.com/vanfanel/SDL-1.2.15-raspberrypi"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~arm"
IUSE=""

DEPEND=""
RDEPEND="media-libs/raspberrypi2-userland
		 media-libs/alsa-lib"

src_configure() {
	econf  --disable-video-opengl \
	       --enable-video-dispmanx \
		   --disable-video-directfb \
		   --disable-oss \
		   --disable-alsatest \
		   --disable-pulseaudio \
		   --disable-pulseaudio-shared \
		   --disable-arts \
		   --disable-nas \
		   --disable-esd \
		   --disable-nas-shared \
		   --disable-diskaudio \
		   --disable-dummyaudio \
		   --disable-mintaudio \
		   --disable-video-x11 \
		   --prefix="/usr" \
		   --disable-input-tslib

}

src_install() {
	default
}

