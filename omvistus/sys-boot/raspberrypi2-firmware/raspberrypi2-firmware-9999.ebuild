# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit git-r3

DESCRIPTION="Raspberry PI2 boot loader and firmware"
HOMEPAGE="https://github.com/raspberrypi/firmware"
EGIT_REPO_URI=${HOMEPAGE}
EGIT_BRANCH="master"

LICENSE="GPL-2 raspberrypi-videocore-bin"
SLOT="0"
KEYWORDS="~arm"
IUSE=""

DEPEND=""
RDEPEND="!sys-boot/raspberrypi-loader"

RESTRICT="binchecks strip"

src_prepare() {
	rm boot/{bcm2708-rpi-b-plus.dtb,bcm2708-rpi-b.dtb,kernel.img,kernel7.img} || die
}

src_install() {
	mv boot ${D} || die
	cp ${FILESDIR}/*.txt ${D}/boot || die
}
