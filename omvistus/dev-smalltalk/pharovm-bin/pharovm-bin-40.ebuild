# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $ 

inherit base eutils

PHARO_HOME="/usr/lib/pharo"
DESCRIPTION="Pharo Smalltalk unix virtual machine"
HOMEPAGE="http://www.pharo-project.org"
SRC_URI="http://files.pharo.org/get-files/${PV}/pharo-linux-stable.zip"
LICENSE="Apple"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

DEPEND="x11-libs/libX11 x11-libs/libXext x11-libs/libXt"
RDEPEND="${DEPEND}"
RESTRICT=mirror

src_install() {
	dodir ${PHARO_HOME} 
	insinto ${PHARO_HOME}
	doins *
	dosym ${PHARO_HOME}/pharo usr/bin/pharo
	chmod +x ${D}/${PHARO_HOME}/pharo
}
